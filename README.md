## Parking api - project setup

## XAMPP
Install XAMPP or similar software to get PHP and MySQL on the system

Go to localhost/phpmyadmin and create a new database 

## Composer
Install composer globally on the system

## ENV
Copy the .env.example file to .env and setup the following values as in this example:

DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=database_name  
DB_USERNAME=database_username  
DB_PASSWORD=database_password  

## Composer install
In the console from the app directory run the following command to get all the dependencies

composer install

## Application key
php artisan key:generate

## Migrate the database
php artisan migrate --seed

## Setup passport
php artisan passport:install

## Serve the aplication
php artisan serve

And that's it. API should now be up and running
