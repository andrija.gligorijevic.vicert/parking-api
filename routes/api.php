<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/'], function () {

    Route::get('/', function () { return ['api' => 'Parking Api', 'version' => 1];});

    /**
     * Sign up
     * {
        "name": "Andrija",
        "email": "andrija.gligorijevic@vicert.com",
        "password": "test1234"
        }
     */
    Route::post('/signup', 'App\Http\Controllers\AuthController@signup');
    /**
     * Log in
     * {
        "email": "andag@vicert.com",
        "password": "test1234"
        }
     */
    Route::post('/login', 'App\Http\Controllers\AuthController@login');

    /**
     * Get reservations by date
     *  date format: '2019-03-25'
     */
    Route::get('/reservations/{date}', 'App\Http\Controllers\ReservationController@reservationsByDate');

    Route::post('/forgot-password', 'App\Http\Controllers\AuthController@forgotPassword');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/me', 'App\Http\Controllers\AuthController@me');
        /**
         * Update user profile
         *
         */
        Route::put('/me', 'App\Http\Controllers\AuthController@updateMe');

        // User crud endpoints for reservations
        /**
         * Reservations
         * {
            "spot_id": 2,
            "reservation_date": "2021-03-09"
            }
         */
        Route::post('/reservations', 'App\Http\Controllers\ReservationController@store');
        Route::delete('/reservations/{reservation}', 'App\Http\Controllers\ReservationController@revokeReservation');

        /**
         * Change password
         *
         */
        Route::patch('/password', 'App\Http\Controllers\AuthController@updatePassword');

        // Car makes and models
        Route::get('/cars/makes', 'App\Http\Controllers\CarController@getMakes');
        Route::get('/cars/models/{make}', 'App\Http\Controllers\CarController@getModels');

        // Car crud endpoints
        /**
         * Cars
         * {
            "make": "Fiat",
            "model": "Punto",
            "year": "2001",
            "reg_no": "BG-1279-TT",
            "color": "NAVY_BLUE"
            }
         */
        Route::resource('/cars', 'App\Http\Controllers\CarController');


        Route::group(['prefix' => '/admin', 'middleware' => \App\Http\Middleware\AdminMiddleware::class], function () {
            // Users endpoints
            // additional query params: search, page
            Route::get('/users', 'App\Http\Controllers\UserController@index');
            Route::delete('/users/{user}', 'App\Http\Controllers\UserController@destroy');
            Route::patch('/users/{user}/handicap', 'App\Http\Controllers\UserController@toggleHandicap');
            Route::patch('/users/{user}/role/', 'App\Http\Controllers\UserController@toggleAdmin');
            /**
             * Update user quota
             * { "quota": 5 }
             */
            Route::put('/users/{user}/quota', 'App\Http\Controllers\UserController@updateQuota');

            // Spots crud endpoints for spots
            /**
             * { "type": "normal" }
             */
            Route::resource('/spots', 'App\Http\Controllers\SpotController');

            // Admin crud endpoints for reservations
            Route::get('/reservations', 'App\Http\Controllers\ReservationController@index' );
            Route::get('/reservations/{reservation}', 'App\Http\Controllers\ReservationController@show' );
            Route::delete('/reservations/{reservation}', 'App\Http\Controllers\ReservationController@destroy');
        });

    });
});

