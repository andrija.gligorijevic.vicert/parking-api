<?php

namespace Database\Factories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    const CAR_COLORS = ['NAVY_BLUE', 'RED', 'BLUE', 'YELLOW', 'GREEN', 'TURQUOISE'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'make' => 'NA',
            'model' => 'NA',
            'year' => 'NA',
            'reg_no' => 'BG-'.$this->faker->unique()->numberBetween(100, 9999).'-NA',
            'color' => $this->faker->randomElement(self::CAR_COLORS)
        ];
    }
}
