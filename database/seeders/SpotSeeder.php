<?php

namespace Database\Seeders;

use App\Models\Spot;
use Illuminate\Database\Seeder;

class SpotSeeder extends Seeder
{
    protected $spots = [
        ['type' => 'normal'],
        ['type' => 'normal'],
        ['type' => 'normal'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->spots as $spot) {
            Spot::create($spot);
        }
    }
}
