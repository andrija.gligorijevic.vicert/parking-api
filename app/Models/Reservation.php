<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = [
        'reservation_date',
        'spot_id',
        'user_id'
    ];

    public function spot() {
        return $this->belongsTo(Spot::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'reservation_date' => $this->reservation_date,
            'spot' => $this->spot,
            'user' => $this->user
        ];
    }
}
