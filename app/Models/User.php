<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'date_of_birth'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function cars() {
        return $this->hasMany(Car::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'date_of_birth' => $this->date_of_birth,
            'handicap' => $this->handicap,
            'role' => $this->role,
            'quota' => $this->quota,
            'car' => $this->cars()->first()
        ];
    }
}
