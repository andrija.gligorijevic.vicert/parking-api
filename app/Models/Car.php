<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    public $fillable = [
        'user_id',
        'make',
        'model',
        'year',
        'reg_no',
        'color'
    ];

    public function toArray()
    {
        return [
            'id' => $this->id,
            'make' => $this->make,
            'model' => $this->model,
            'year' => $this->year,
            'reg_no' => $this->reg_no,
            'color' => $this->color
        ];
    }
}
