<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users|regex:/vicert.com$/',
            'date_of_birth' => 'date',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'email.regex' => 'Email must be from the vicert.com domain'
        ];
    }
}
