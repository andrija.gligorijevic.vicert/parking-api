<?php

namespace App\Http\Requests;

use App\Models\Car;
use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'make' => 'string|required',
            'model' => 'string|nullable',
            'year' => 'string|nullable|min:4|max:4',
            'reg_no' => 'string|required|unique:cars',
            'color' => 'string',
        ];
    }
}
