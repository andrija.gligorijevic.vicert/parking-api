<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->guard('api')->user();
        return [
            'name' => 'string',
            'email' => 'email|unique:users,email,'.$user->id.'|regex:/vicert.com$/',
            'date_of_birth' => 'date|nullable',
        ];
    }

    public function messages()
    {
        return [
            'email.regex' => 'Email must be from the vicert.com domain'
        ];
    }
}
