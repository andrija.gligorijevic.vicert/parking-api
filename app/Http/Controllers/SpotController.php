<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpotRequest;
use App\Models\Spot;
use Illuminate\Http\Response;

class SpotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Spot::all(), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpotRequest $request)
    {
        $spot = Spot::create($request->all());

        return response($spot, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Spot  $spot
     * @return \Illuminate\Http\Response
     */
    public function show(Spot $spot)
    {
        return response($spot, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Spot  $spot
     * @return \Illuminate\Http\Response
     */
    public function update(SpotRequest $request, Spot $spot)
    {
        $spot->update($request->all());

        return response($spot, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Spot $spot
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Spot $spot)
    {
        $spot->delete();

        return response([], Response::HTTP_OK);
    }
}
