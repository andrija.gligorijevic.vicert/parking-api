<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReservationRequest;
use App\Models\Reservation;
use App\Models\Spot;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Reservation::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservationRequest $request)
    {
        $userId = auth()->guard('api')->user()->getAuthIdentifier();

        $reservationParams = $request->only(['reservation_date', 'spot_id']);

        // Check whether the spot is already taken
        $checkReservations = Reservation::where($reservationParams)->get();
        if (count($checkReservations)) {
            return response(['message' => 'Already taken'], Response::HTTP_CONFLICT);
        }

        // Check whether the user already reserved a spot that day
        $checkReservations = Reservation::where(array_merge($request->only('reservation_date'), ['user_id' => $userId]))->get();
        if (count($checkReservations)) {
            return response(['message' => 'You can only reserve one spot per day'], Response::HTTP_CONFLICT);
        }

        $reservationData = array_merge(
            $reservationParams,
            [
                'user_id' => $userId
            ]
        );

        $reservation = Reservation::create($reservationData);

        return response($reservation, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return response($reservation, Response::HTTP_OK);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Reservation $reservation
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();

        return response([], Response::HTTP_OK);
    }

    /**
     * Fetch reservations for a specific date
     * @param $date
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function reservationsByDate($date) {
        $reservations = Reservation::where('reservation_date', $date)->get();

        $spots = Spot::all();
        $spotsArray = [];
        foreach ($spots as $spot) {
            $spotsArray[] = [
                'spot_id' => $spot->id,
                'spot_type' => $spot->type,
                'reservation' => Reservation::where(['reservation_date' => $date, 'spot_id' => $spot->id])->first()
            ];
        }

        return response($spotsArray, Response::HTTP_OK);
    }

    /**
     * Remove a reservation you made
     * @param Reservation $reservation
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function revokeReservation(Reservation $reservation) {
        $userId = auth()->guard('api')->user()->getAuthIdentifier();
        if ($reservation->user_id !== $userId) {
            response(['message' => 'You cannot remove another users reservation'], Response::HTTP_FORBIDDEN);
        }

        $reservation->delete();

        return response([], Response::HTTP_OK);
    }
}
