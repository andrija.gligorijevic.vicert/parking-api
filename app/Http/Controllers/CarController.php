<?php

namespace App\Http\Controllers;

use App\Helper\Cars;
use App\Http\Requests\CarRequest;
use App\Models\Car;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Response;

class CarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = auth()->guard('api')->user()->getAuthIdentifier();
        $cars = Car::where('user_id', $userId)->get();

        return response($cars, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        $userId = auth()->guard('api')->user()->getAuthIdentifier();

        $car = Car::create(array_merge(
            ['user_id' => $userId],
            $request->only(['make', 'model', 'year', 'reg_no', 'color'])
        ));

        return response($car, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return response($car, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(CarRequest $request, Car $car)
    {
        if (! Gate::allows('update-car', $car)) {
            return response(['message' => 'You cannot update another user\'s car'], Response::HTTP_CONFLICT);
        }

        $car->update( $request->only(['make', 'model', 'year', 'reg_no', 'color']));

        return response([], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Car $car
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Car $car)
    {
        if (! Gate::allows('update-car', $car)) {
            return response(['message' => 'You cannot delete another user\'s car'], Response::HTTP_CONFLICT);
        }

        if (count(auth()->guard('api')->user()->cars) === 1) {
            return response(['message' => 'You cannot delete your only car'], Response::HTTP_CONFLICT);
        }

        $car->delete();

        return response([], Response::HTTP_OK);
    }

    public function getMakes() {
        $makes = Cars::getCarMakes();

        return response($makes, Response::HTTP_OK);
    }

    public function getModels($make) {
        $models = Cars::getCarModels($make);

        return response($models, Response::HTTP_OK);
    }
}
