<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Car;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    public function signup(SignupRequest $request) {

        $user = User::make($request->only([
            'name',
            'email',
            'date_of_birth',
            'password'
        ]));
        $user->password = Hash::make($request->password);
        $user->save();

        Car::factory()->create(['user_id' => $user->id]);

        Auth::attempt($request->only(['email', 'password']));

        $token = Auth::user()->createToken('authToken');

        return response([
            'user' => Auth::user(),
            'api_token' => $token
        ], Response::HTTP_CREATED);
    }

    public function login(Request $request) {

        if (! Auth::attempt($request->only(['email', 'password']))) {
            return response(['message' => 'Invalid email or password'], Response::HTTP_BAD_REQUEST);
        }

        $token = Auth::user()->createToken('authToken');

        return response([
            'user' => Auth::user(),
            'api_token' => $token
        ], Response::HTTP_OK);
    }

    public function me(Request $request) {
        $user = auth()->guard('api')->user();

        return response([
            'user' => $user
        ], Response::HTTP_OK);
    }

    public function updateMe(UpdateUserRequest $request) {
        $user = auth()->guard('api')->user();

        $user->update($request->only(['name', 'email', 'date_of_birth']));

        return response([], Response::HTTP_OK);
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $user = auth()->guard('api')->user();

        if(!\Hash::check($request->input('old_password'), $user->password)) {
            return response(['message' => 'Old password does not match'], Response::HTTP_BAD_REQUEST);
        }

        $user->password = Hash::make($request->input('new_password'));
        $user->save();

        return response([], Response::HTTP_OK);
    }

    public function forgotPassword(Request $request) {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return response($status, Response::HTTP_OK);
    }

    public function resetPassword(Request $request) {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();
            }
        );

        return response($status, Response::HTTP_OK);
    }
}
