<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateQuotaRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index(Request $request) {

        $searchTerm = $request->input('search');
        $users = new User();
        if ($searchTerm) {
            $users = $users->where('name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        }

        $users = $users->paginate(4);

        if ($searchTerm) {
            $users->appends(['search' => $searchTerm]);
        }

        return response($users, Response::HTTP_OK);
    }

    public function destroy(User $user) {
        $user->delete();

        return response([], Response::HTTP_OK);
    }

    public function toggleHandicap(User $user) {
        $user->handicap = !$user->handicap;
        $user->save();

        return response($user, Response::HTTP_OK);
    }

    public function toggleAdmin(User $user) {
        $user->role = $user->role === 'user' ? 'admin' : 'user';
        $user->save();

        return response($user, Response::HTTP_OK);
    }

    public function updateQuota(User $user, UpdateQuotaRequest $request) {
        $user->quota = $request->input('quota');
        $user->save();

        return response($user, Response::HTTP_OK);
    }
}
